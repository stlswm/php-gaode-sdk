<?php

namespace stlswm\PHPGaoDeSdk;

class Response
{
    /**
     * @var bool $result
     */
    public bool $result = false;

    /**
     * @var string $errMsg
     */
    public string $errMsg = '';

    /**
     * @var mixed 返回数据
     */
    public $data = [];
}