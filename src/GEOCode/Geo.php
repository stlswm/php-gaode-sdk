<?php

namespace stlswm\PHPGaoDeSdk\GEOCode;

use Exception;
use stlswm\PHPGaoDeSdk\BaseRequest;

/**
 * 地理编码
 * https://lbs.amap.com/api/webservice/guide/api/georegeo
 */
class Geo extends BaseRequest
{
    const URL = 'https://restapi.amap.com/v3/geocode/geo';

    const HttpMethod = 'get';

    protected string $address;
    protected string $city;
    protected string $output;//只能是json不能修改
    protected string $callback;

    public function __construct()
    {
        $this->biz_list[] = 'output';
        $this->output = 'JSON';
    }

    /**
     * 设置业务参数
     * @param  string  $name
     * @param  mixed   $value
     * @throws Exception
     */
    public function setBusinessParam(string $name, $value)
    {
        if (!property_exists($this, $name) || $name == 'output') {
            throw new Exception('无效参数：'.$name);
        }
        $this->biz_list[] = $name;
        $this->$name = $value;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isValid(): bool
    {
        $mustFields = [
            'address',
        ];
        foreach ($mustFields as $field) {
            if (empty($this->$field)) {
                throw new Exception("param {$field} must be set");
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function exportBusinessParam(): array
    {
        $params = [];
        foreach ($this->biz_list as $column) {
            $params[$column] = $this->$column;
        }
        return $params;
    }
}