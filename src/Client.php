<?php

namespace stlswm\PHPGaoDeSdk;

use Exception;

class Client
{
    use CurlHttp;

    public string $key;

    /**
     * https://lbs.amap.com/faq/quota-key/key/41169
     * @var string gaode高德数字签名
     */
    public string $sign;

    /**
     * @param  BaseRequest  $request
     * @param  string       $url     请求地址
     * @param  string       $method  请求方式
     * @return Response
     * @throws Exception
     */
    public function exec(BaseRequest $request, string $url, string $method = 'post'): Response
    {
        $method = strtolower($method);
        //参数验证
        if (!$request->isValid()) {
            throw new Exception('参数验证失败');
        }
        //公共参数
        $params = [];
        $params['key'] = $this->key;
        if (!empty($this->sign)) {
            $params['sig'] = $this->sign;
        }
        $params = array_merge($params, $request->exportBusinessParam());
        switch ($method) {
            case 'get':
                return self::get($url, $params);
            case 'post':
                return self::post($url, $params);
            default:
                throw new Exception('unknown method:'.$method);
        }
    }
}