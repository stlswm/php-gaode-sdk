<?php

namespace stlswm\PHPGaoDeSdk\Direction;

use Exception;
use stlswm\PHPGaoDeSdk\BaseRequest;

/**
 * 驾车路径规划
 * https://lbs.amap.com/api/webservice/guide/api/direction
 */
class Driving extends BaseRequest
{
    const URL        = 'https://restapi.amap.com/v3/direction/driving';
    const HttpMethod = 'get';

    //起点经纬度;经度在前，纬度在后，经度和纬度用","分割，经纬度小数点后不得超过6位。
    protected string $origin;
    //目的地;经度在前，纬度在后，经度和纬度用","分割，经纬度小数点后不得超过6位。
    protected string $destination;
    protected string $originid;
    protected string $destinationid;
    protected string $origintype;
    protected string $destinationtype;
    protected string $strategy;
    protected string $waypoints;
    protected string $avoidpolygons;
    protected string $avoidroad;
    protected string $province;
    protected string $number;
    protected string $cartype;
    protected string $ferry;
    protected string $roadaggregation;
    protected string $nosteps;
    protected string $output;
    protected string $callback;
    protected string $extensions;

    public function __construct()
    {
        $this->biz_list[] = 'output';
        $this->output = 'JSON';
        $this->biz_list[] = 'extensions';
        $this->extensions = 'base';
    }

    /**
     * 设置业务参数
     * @param  string  $name
     * @param  mixed   $value
     * @throws Exception
     */
    public function setBusinessParam(string $name, $value)
    {
        if (!property_exists($this, $name) || $name == 'output') {
            throw new Exception('无效参数：'.$name);
        }
        $this->biz_list[] = $name;
        $this->$name = $value;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isValid(): bool
    {
        $mustFields = [
            'origin',
            'destination',
        ];
        foreach ($mustFields as $field) {
            if (empty($this->$field)) {
                throw new Exception("param {$field} must be set");
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function exportBusinessParam(): array
    {
        $params = [];
        foreach ($this->biz_list as $column) {
            $params[$column] = $this->$column;
        }
        return $params;
    }
}