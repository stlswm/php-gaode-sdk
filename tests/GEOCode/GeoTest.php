<?php

namespace tests\GEOCode;

use Exception;
use PHPUnit\Framework\TestCase;
use stlswm\PHPGaoDeSdk\Client;
use stlswm\PHPGaoDeSdk\GEOCode\Geo;
use tests\Config;

require '../../vendor/autoload.php';
require '../Config.php';

class GeoTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testReq()
    {
        $geo = new Geo();
        $geo->setBusinessParam('address', '成都市郫都区阳光北路');
        $client = new Client();
        $client->key = Config::KEY;
        $res = $client->exec($geo, Geo::URL, Geo::HttpMethod);
        print_r($res);
        $this->assertEquals(true, $res->result);
    }
}