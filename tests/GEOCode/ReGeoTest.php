<?php

namespace tests\GEOCode;

use Exception;
use PHPUnit\Framework\TestCase;
use stlswm\PHPGaoDeSdk\Client;
use stlswm\PHPGaoDeSdk\GEOCode\Geo;
use stlswm\PHPGaoDeSdk\GEOCode\ReGeo;
use tests\Config;

require '../../vendor/autoload.php';
require '../Config.php';

class ReGeoTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testReq()
    {
        $geo = new ReGeo();
        $geo->setBusinessParam('location', '104.439554,30.854302');
        $geo->setBusinessParam('extensions', 'all');
        $client = new Client();
        $client->key = Config::KEY;
        $res = $client->exec($geo, ReGeo::URL, Geo::HttpMethod);
        print_r($res);
        $this->assertEquals(true, $res->result);
    }
}