<?php

namespace tests\Direction;

use Exception;
use PHPUnit\Framework\TestCase;
use stlswm\PHPGaoDeSdk\Client;
use stlswm\PHPGaoDeSdk\Direction\Driving;
use tests\Config;

require '../../vendor/autoload.php';
require '../Config.php';

class DrivingTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testReq()
    {
        $driving = new Driving();
        $driving->setBusinessParam('origin', '104.062481,30.731686');
        $driving->setBusinessParam('destination', '104.027200,30.745365');
        $client = new Client();
        $client->key = Config::KEY;
        $res = $client->exec($driving, Driving::URL, Driving::HttpMethod);
        print_r($res);
        $this->assertEquals(true, $res->result);
    }
}